@extends('layout.master')

 @section('tema')
   FORM
 @endsection

 @section('isi')
   <h1>FORM MEDIA ONLINE</h1>
   <h2>Sign Up Form</h2>
   <form action="/welcome1" method="post">
        @csrf
            <label for="">First Name :</label>
            <br> <br>        
            <input type="text" name1="nama">
            <br><br>
            <label for="">Last Name :</label>
            <br><br>
            <input type="text" name2="nama">
            <br><br>
            <label>Gender :</label> <br><br>
            <input type="radio" name="kelamin" value="M" checked> Male <br>
            <input type="radio" name="kelamin" value="F"checked> Female <br><br>
            <label>Nationality</label>
                <select>
                    <option value="indonesia">Indonesian</option>
                    <option value="Inggris">English</option>
                    <option value="Arab">Arabian</option>
                    <option value="Lainnya">Other</option>
                </select><br>.<br>
            <label>Language Spoken :</label><br><br>
            <input type="checkbox"> Bahasa Indonesia<br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br><br>
            <label for="Isi">Bio</label>
            <br>
            <textarea cols="30" rows="10" id="Isi"></textarea><br>
        </form>
        <a href="Welcome.html">Sign Up</a>
@endsection            
